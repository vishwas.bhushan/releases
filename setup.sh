# reference https://stackoverflow.com/a/28938235/1851064
GREEN='\033[0;32m'
RED='\033[0;31m'
BLUE='\033[0;34m'
BLUE_BG='\033[44m'

NC='\033[0m' # No Color

##
# CONSTRUCT THE DIRECTORY STRUCTURE
##
echo -e "${BLUE}Creating directory structure${NC}"
cd ~ > /dev/null # ~/
mkdir -p stellar/git
mkdir -p stellar/ledger
mkdir -p stellar/downloads
mkdir -p tools/horizon

##
# CLONE THE REQUIRED GIT REPOSITORY
##
echo -e "${BLUE}Cloning git repositories${NC}"
cd stellar/git > /dev/null # ~/stellar/git
git clone https://gitlab.com/npci-stellar/stellar-core.git
git clone https://gitlab.com/npci-stellar/stellar-cluster.git
git clone https://gitlab.com/npci-stellar/dashboard.git

##
# INSTALL THE APPLICATIONS
##
echo -e "${BLUE}Setting up repositories${NC}"

# STELLAR-CORE
echo -e "${BLUE_BG}stellar-core${NC}"
if [[ -z $(stellar-core version) ]]; then
    cd stellar-core > /dev/null # ~/stellar/git/stellar-core
    git submodule init
    git submodule update
    ./autogen.sh
    ./configure
    # ref: https://stackoverflow.com/a/15295032/1851064
    make -j6
    sudo make install
    stellar-core version # to verify the installation
    cd - > /dev/null  # ~/stellar/git
fi
echo -e "${BLUE}done${NC}"
# ---------------------

# HORIZON
echo -e "${BLUE_BG}horizon${NC}"
cd ~/stellar/downloads > /dev/null  # ~/stellar/downloads
read -p 'enter latest horizon version number: [v0.11.1] ' HORIZON_VERSION;
if [[ $HORIZON_VERSION == "" ]]; then
    HORIZON_VERSION="v0.11.1"
fi

FILE="~/stellar/downloads/$HORIZON_VERSION/horizon-$HORIZON_VERSION-darwin-amd64.tar.gz"
if [[ -f "$FILE" ]]; then
    echo -e "${BLUE}tar exists at location: $FILE"
else
    wget "https://github.com/stellar/horizon/releases/download/$HORIZON_VERSION/horizon-$HORIZON_VERSION-darwin-amd64.tar.gz"
    cd - > /dev/null  # ~/stellar/git
    cd ~/tools  # ~/tools/horizon
    tar -zxvf ~/stellar/downloads/horizon-$HORIZON_VERSION-darwin-amd64.tar.gz -C horizon --strip-components 1
    cd - > /dev/null  # ~/stellar/git
fi
REMINDER="${BLUE_BG}        ${NC}\n${BLUE_BG}REMINDER${NC}\n${BLUE_BG}        ${NC}\n${BLUE_BG}1.${NC} Set ~/tools/horizon to path variable and check if horizon is working (run horizon version)\n"
# ---------------------

# POSTGRES
echo -e "${BLUE_BG}postgress${NC}"
sudo apt install postgresql postgresql-contrib
sudo -u postgres psql -c "CREATE USER stellar_node_user WITH PASSWORD 'testdb';"
sudo -u postgres psql -c "ALTER USER stellar_node_user WITH SUPERUSER;"
# ---------------------

##
# COPY CONFIG FILES
##
cd ~/stellar/ledger > /dev/null # ~/stellar/ledger
cp ~/stellar/git/stellar-cluster/config/stellar-core.cfg .
cd - > /dev/null # ~/stellar/git

##
# INSTALL NPM PACKAGES
##
echo -e "${BLUE_BG}npm install${NC}"

# REMITTER
cd remitter > /dev/null # ~/stellar/git/remitter
npm i
cd - > /dev/null # ~/stellar/git

# BENEFICIARY
cd beneficiary > /dev/null # ~/stellar/git/beneficiary
npm i
cd - > /dev/null # ~/stellar/git

# QUEUE SERVICE
cd queue_service/ > /dev/null # ~/stellar/git/queue_service/
npm i
cd - > /dev/null # ~/stellar/git

# STELLAR API
cd stellar_api/ > /dev/null # ~/stellar/git/stellar_api/
npm i
cd - > /dev/null # ~/stellar/git

REMINDER=$REMINDER"${BLUE_BG}2.${NC} Run stellar-core gen-seed and use the secret seed as NODE_SEED in the ~/stellar/git/stellar-cluster/config/stellar-core.cfg\n"

echo -e $REMINDER